FROM rustdocker/rustup AS build
SHELL ["/bin/bash", "-l", "-c"]
RUN apt update -yq && apt install -yq musl-tools
RUN rustup default stable
RUN rustup target add x86_64-unknown-linux-musl
WORKDIR /pkgs
COPY . .
RUN cargo build --target x86_64-unknown-linux-musl --target-dir build -r --manifest-path obsidian-export/Cargo.toml
RUN cargo build --target x86_64-unknown-linux-musl --target-dir build -r --manifest-path zola/Cargo.toml

FROM alpine:3.18
WORKDIR /app
RUN apk add --no-cache inotify-tools
COPY --from=build /pkgs/build/x86_64-unknown-linux-musl/release/zola /bin/
COPY --from=build /pkgs/build/x86_64-unknown-linux-musl/release/obsidian-export /bin/


